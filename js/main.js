/*
 * @Date: 2022-07-23 11:07:18
 * @LastEditors: kirin
 * @LastEditTime: 2022-08-27 17:49:02
 * @FilePath: \新个人网站\js\main.js
 */
/* 
    一些公共的函数和功能
    ==================================
  [目录]

    1. 页面初始化，计算rem
    2. 导航条加亮
    3. 回到顶部
    4. 封装的常用函数
*/

/*--------------------------------------------------------------
  1. 页面初始化，计算rem
--------------------------------------------------------------*/
(function () {
  //页面初始化，针对屏幕不是1920*1080的页面尺寸
  const buildRem = function () {
    let designSize = 1920; // 设计图尺寸            
    let html = document.documentElement;
    let wW = html.clientWidth;// 窗口宽度
    let rem = wW * 100 / designSize;
    html.style.fontSize = rem + 'px';
  }
  // 页面初始化的时候调用
  buildRem()
  // 页面缩放时调用
  window.onresize = buildRem;
  //设置不能滚动
  // document.documentElement.style.overflow='hidden';
})();

/*--------------------------------------------------------------
  2. 导航条加亮
--------------------------------------------------------------*/

(function () {
  const header = document.querySelector('#header');
  if (header) {
    const navData = [
      {
        title: "首页",
        url: "index.html",
      },
      {
        title: "自我介绍",
        url: "self.html",
      },
      {
        title: "学校生活",
        url: "schoolLife.html",
      },
      {
        title: "旅游风景",
        url: "journey.html",
      },
      {
        title: "书籍",
        url: "book.html",
      },
      {
        title: "电影",
        url: "movie.html"
      }
    ];
    const insertNav = function () {
      let res = ''
      navData.forEach(nav => {
        if (getPageName() == "blogSingle.html" || "") {
          if (nav.url != "blog.html") {
            res += `<li class="${getPageName() == nav.url ? 'active' : ''}"  ><a href="${nav.url}">${nav.title}</a></li>`
          } else {
            res += `<li class="active"><a href="${nav.url}">${nav.title}</a></li>`
          }
          if (getPageName() == "") {
            if (nav.url != "index.html") {
              res += `<li class="${getPageName() == nav.url ? 'active' : ''}"  ><a href="${nav.url}">${nav.title}</a></li>`
            } else {
              res += `<li class="active"><a href="${nav.url}">${nav.title}</a></li>`
            }
          }
        } else {
          res += `<li class="${getPageName() == nav.url ? 'active' : ''}"  ><a href="${nav.url}">${nav.title}</a></li>`
        }
      })
      return res
    }

    header.insertAdjacentHTML('afterbegin', `
      <div class="container flex align-items-center justify-content-between">
        <a href="./index.html"><img class="logo" src="./image/logo.png" alt=""></a>
        <nav id="navbar" class="navbar">
          <ul>${insertNav()}</ul>
        </nav>
      </div>
    `)
    function getPageName() {
      const pathArr = location.pathname.split('/')
      return pathArr[pathArr.length - 1]
    }
  }
})()

/*--------------------------------------------------------------
  3. 回到顶部
--------------------------------------------------------------*/
const oBack = document.querySelector("#back")
const htmlEle = document.documentElement

oBack.addEventListener("click", function () {
  htmlEle.scrollTo({
    top: 0,
    behavior: "smooth"
  });
}, false)
/*--------------------------------------------------------------
  4. loading
--------------------------------------------------------------*/
  // window.addEventListener = ("load", function () {
    let preloader = document.querySelector('#preloader');
  //   console.log(1);
  //   preloader.style.display = 'none';
  //   console.log(2);
  // }, false)
  let loading = setInterval(()=>{
    preloader.style.opacity = '0';
    preloader.style.visibility = 'hidden';
  },500)
/*--------------------------------------------------------------
  5. 常用函数封装
--------------------------------------------------------------*/
function main(global) {
  global = global || self;

  let baseUrl = "http://81.70.162.221:3000/";
  // let baseUrl = "http://43.143.62.195:8092";
  axios.defaults.baseURL = baseUrl;

  global.imgUrl = function (url) {
    return baseUrl + url;
  };

  global.getQueryString = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
  };
  //定义替换参数的方法
  global.changeURLArg = function (url, arg, arg_val) {
    var pattern = arg + '=([^&]*)';
    var replaceText = arg + '=' + arg_val;
    if (url.match(pattern)) {
      var tmp = '/(' + arg + '=)([^&]*)/gi';
      tmp = url.replace(eval(tmp), replaceText);
      return tmp;
    } else {
      if (url.match('[\?]')) {
        return url + '&' + replaceText;
      } else {
        return url + '?' + replaceText;
      }
    }
    return url + '\n' + arg + '\n' + arg_val;
  }
  

}
main(window);
// 时间格式：Y-M-D
function time(e) {
  let time = e.split("T")[0]
  return time
}