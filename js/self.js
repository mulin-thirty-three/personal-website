/*
 * @Date: 2022-08-07 13:11:41
 * @LastEditors: kirin
 * @LastEditTime: 2022-08-10 12:01:21
 * @FilePath: \新个人网站\js\about.js
 */
(function(){
  const loadingWList = document.querySelectorAll(".loadingW")
  const percentList = document.querySelectorAll(".percent")
  let num = 0
  HTML = setInterval(() => {
    runProgress(100,HTML,percentList[0],loadingWList[0])
  }, 50)

  css = setInterval(() => {
    runProgress(70,css,percentList[1],loadingWList[1])
  }, 80)

  js = setInterval(() => {
    runProgress(50,js,percentList[2],loadingWList[2])
  }, 100)

  java = setInterval(() => {
    runProgress(60,java,percentList[3],loadingWList[3])
  }, 80)

  c = setInterval(() => {
    runProgress(40,c,percentList[4],loadingWList[4])
  }, 80)
  
  node = setInterval(() => {
    runProgress(20,node,percentList[5],loadingWList[5])
  },80)

  function runProgress(e,clear,inner,w) {
    if (num < e) {
      num += 1
      inner.innerHTML = num + "%"
      w.style.width = num + "%";
    }else { 
      clearInterval(clear);
      console.log(e);
    }
  }
})()
  