/*
 * @Date: 2022-07-29 15:19:58
 * @LastEditors: kirin
 * @LastEditTime: 2022-09-24 14:38:02
 * @FilePath: \新个人网站\js\blog.js
 */
(function () {
  /* 粒子库部分 */
  particlesJS('particles-js',

    {
      "particles": {
        "number": {
          "value": 80,
          "density": {
            "enable": true,
            "value_area": 400
          }
        },
        "color": {
          "value": "random"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 5,
            "color": "random"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 100,
            "height": 100
          }
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 5,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#ffffff",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 6,
          "direction": "none",
          "random": false,
          "straight": false,
          "out_mode": "out",
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "repulse"
          },
          "onclick": {
            "enable": true,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 400,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true,
      "config_demo": {
        "hide_card": false,
        "background_color": "#b61924",
        "background_image": "",
        "background_position": "50% 50%",
        "background_repeat": "no-repeat",
        "background_size": "cover"
      }
    }
  );/* END 粒子库 */

  const articleWrap = document.querySelector('#blog .article-wrap')
  const searchInput = document.querySelector('#search-input')
  const searchBtn = document.querySelector('#search-btn')
  let pageNum = location.search.split("=")[1] || 1

  let totalPage = 0

  searchBtn.addEventListener('click', function () {
    let q = searchInput.value.trim();
    fetchData(q)
  }, false)
  // get(url).then(callback);
  fetchData()

  function fetchData(q) {
    // axios.get('http://43.143.62.195:8092/TArticle/getAllArticles', {
      axios.get('http://81.70.162.221:3000/getBlogs', {
      params: {
        pageNum: pageNum,
        pageSize: 6,/* 每页几条信息 */
        keyword: q,
      }
    }).then(function (response) {
      articleWrap.innerHTML = ""
      // 分页
      // totalPage = response.data.pageTotal
      totalPage = response.data.totalPage
      inintialPagination()
      // blog
      // console.log(response.data.empty);
      // let blogs = response.data.articleList;
      console.log(response);
      let blogs = response.data.data;
      if(response.data.empty){
        alert("没搜到该关键字的相关内容")
        searchInput.value = ""
        fetchData()
      }
      blogs.forEach(blog => {
        articleWrap.insertAdjacentHTML('beforeend', `
        <article>
        <a href="blogSingle.html?id=${blog.id}"><img src="http://81.70.162.221:3000/${blog.image}" alt=""></a>
        <div class="content-wrap">
          <a href="blogSingle.html?id=${blog.id}">
            <h3>${blog.title}</h3>
          </a>
          <div class="blog-content"><p>${blog.abstract} </p></div>
          <div class="flex justify-content-between align-items-center">
            <span class="date"><img class="clock" src="./image/clock.png">${time(blog.createTime)}</span>
            <a class="more btn-style" href="blogSingle.html?id=${blog.id}">查看更多</a>
          </div>
        </div>
      </article>
    `);
      });
    });
  }

  function inintialPagination() {
    const blogList = document.querySelector("#blog-list")
    blogList.innerHTML = ""
    const prev = document.createElement('a')
    prev.innerHTML = `<img src="./image/blog-prev.png" id="blog-prev">`
    prev.href = `blog.html?pageNum=${prevPage()}`
    blogList.appendChild(prev)
    function prevPage() {
      let newPageNum = pageNum * 1
      if (newPageNum > 1) {
        newPageNum -= 1
        prev.classList.remove("disabled")
      } if (pageNum == "1") {
        prev.classList.add("disabled")
      }
      return newPageNum
    }


    for (let i = 1; i <= totalPage; i++) {
      const a = document.createElement('a')
      a.className = `${pageNum == i ? 'blog-active' : ""}`
      a.innerHTML = `${i}`
      a.href = `blog.html?pageNum=${i}`
      blogList.appendChild(a)
    }

    const next = document.createElement('a')
    next.innerHTML = `<img src="./image/blog-more.png" id="blog-next">`
    next.href = `blog.html?pageNum=${nextPage()}`
    blogList.appendChild(next)
    function nextPage() {
      let newPageNum = pageNum * 1
      if (newPageNum < totalPage) {
        newPageNum += 1
      } if (pageNum == totalPage) {
        next.classList.add("disabled")
      }
      return newPageNum
    }
  }
})()
