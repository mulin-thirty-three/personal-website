/*
 * @Date: 2022-07-27 11:14:19
 * @LastEditors: kirin
 * @LastEditTime: 2022-08-08 20:48:32
 * @FilePath: \新个人网站\js\index.js
 */

/* 轮播图 */
(function () {
  const oCarousel = document.querySelector('.carousel');
  const oImgBox = oCarousel.querySelector('.imgs')
  const oPrev = oCarousel.querySelector('.prev');
  const oNext = oCarousel.querySelector('.next');
  const aImgs = oCarousel.querySelectorAll('.imgs-wrap');
  const moveTimes = aImgs.length - 3
  let currTime = 0
  oPrev.onclick = oNext.onclick = function () {
    if (this == oPrev) {
      if (currTime > 0) {
        currTime--
        changeImg()
      }
    } else {
      if (currTime < moveTimes) {
        currTime++
        changeImg()
      }
    }
    changeColor()
  }
  function changeImg() {
    oImgBox.style.left = -((aImgs[0].offsetWidth + 30) * currTime) + 'px'
  }
  function changeColor() {
    if (currTime <= 0) {
      oPrev.classList.add('invalid')
    } else {
      oPrev.className = "prev";
    }
    if (currTime >= moveTimes) {
      oNext.classList.add('invalid')
    } else {
      oNext.className = "next";
    }
  }
  let timer
  function go() {
    timer = setInterval(() => {
      if (currTime >= moveTimes) {
        currTime = -1
      } else {
        oNext.onclick()
      }
    }, 2000)
  }
  go()
  oCarousel.onmouseover = function () {
    clearInterval(timer)
  }
  oCarousel.onmouseout = function () {
    go()
  }
  blog()
  function blog() {
    axios.get('http://81.70.162.221:3000/getBlogs', {
      params: {
        package: 10,
        pageSize: 6
      }
    }).then(function (res) {
      let blogs = res.data.data
      console.log(blogs);
      const blogBox = document.querySelector(".blog-box")
      blogs.forEach(blog => {
        blogBox.insertAdjacentHTML('beforeend', `
      <li>
        <ul class="blog">
        <img src="http://81.70.162.221:3000/${blog.image}"  style="height: 200px;">
      <li>
        <a href="./blog.html"><h2>${blog.title}</h2></a>
      </li>
      <li>${blog.abstract}</li>
        <li class="flex text">
          <div class="flex">
           <img class="clock" src="./image/clock.png">
            <p class="time">${time(blog.createTime)}</p>
          </div>
          <a href="./blog.html" class="blog-more btn-style">查看更多</a>
        </li>
        </ul>
      </li>
    `)
      });
    })
  }

})()/* END 轮播图 */
